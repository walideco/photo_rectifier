###################### Fichier contenant que les Fonctions ######################
from point import *
from edge import *
from math import *
import numpy as np

#Cette fonction permet de calculer le perimetre du quadrilatere
#di represente un objet droite defini par la classe edge
def  getCircumference(d1,d2,d3,d4):
    m1 = d1.intersection(d2)
    m2 = d2.intersection(d3)
    m3 = d3.intersection(d4)
    m4 = d4.intersection(d1)
    
    return m1.distance(m2)+m2.distance(m3)+m3.distance(m4)+m4.distance(m1)

#Cette fonction de preciser l'espace recherche
#pi point defini par la classe point
#imgDim est une liste des dimensions de l image avec pour
#premier element height et second element width
def  checkSpacing(p1,p2,p3,p4,imgDim):
    
    points =sortPoints ( p1,p2,p3,p4)

    p1 = points[0]
    p2 = points[1]
    p3 = points[2]
    p4 = points[3]
    
    if(p1.distance(p2)+p2.distance(p4)+p4.distance(p3)+p3.distance(p1)<= (imgDim[0]+imgDim[1])/2 ):
        return False    
    if(p1.x > int(imgDim[1]*2/5) or p1.y > int(imgDim[0]*2/5)):
        return False
    if(p2.x < int(imgDim[1]*3/5) or p2.y > int(imgDim[0]*2/5)):
        return False
    if(p3.x > int(imgDim[1]*2/5) or p3.y < int(imgDim[0]*3/5)):
        return False
    if(p4.x < int(imgDim[1]*3/5) or p4.y < int(imgDim[0]*3/5)):
        return False

    return True

#Cette fonction permet de determiner les positions des points pi   
def sortPoints ( p1,p2,p3,p4):

    points = [p1,p2,p3,p4]
    
    for k in range (0,len(points)-1):
        for i in range(0,len(points)-1):
            if( ( points[i].x**2+ points[i].y**2) >  (points[i+1].x**2+ points[i+1].y**2) ) :
                tmp = points[i]
                points[i] = points[i+1]
                points[i+1] = tmp

    
    if(points[1].x < points[2].x):
        tmp = points[1]
        points[1] = points[2]
        points[2] = tmp 

    return points


#Programme du Quickshort pour ranger une liste dans l ordre decroissant
#ou alist est une list ou un tableau ..
def quickSort(alist):
   quickSortHelper(alist,0,len(alist)-1)



def quickSortHelper(alist,first,last):
   if first<last:
       splitpoint = partition(alist,first,last)
       quickSortHelper(alist,first,splitpoint-1)
       quickSortHelper(alist,splitpoint+1,last)


def partition(alist,first,last):
   pivotvalue = alist[first]
   left = first+1
   right = last

   done = False
   while not done:

       while left <= right and alist[left] >= pivotvalue:
           left +=1

       while alist[right] <= pivotvalue and right >= left:
           right -=1

       if right < left:
           done = True
       else:
           temp = alist[left]
           alist[left] = alist[right]
           alist[right] = temp

   temp = alist[first]
   alist[first] = alist[right]
   alist[right] = temp
   return right


#Cette fonction permet de creer un tableau a deux dimension
def imgCreator(height,width):
    alist =[]
    for i in range(height):
        alist.append([0]*width)
    return np.array(alist)

#Fonction permettant de determiner la luminance de l'image
#n represente la taille de la matrice carre
def lumImage(image,width,height,n):
    w = int(width/n)
    h = int(height/n)
    if fmod(width,n) != 0:
        w = w + 1
    if fmod(height,n) != 0:
        h = h + 1
    newImage = imgCreator(h,w)
    c = -1
    for x in range (0,height,n) :
        c=c+1
        d=-1
        for y in range (0,width,n) :
            d=d+1
            u=0
            v=0
            alist = []
            for u in range(n):
                a = x+u
                if a> (height-1):
                    break;
                for v in range(n):
                    b = y+v
                    if b> (width-1):
                        break;
                    alist+=[max(image[a][b][0],image[a][b][1],image[a][b][2])]
            quickSort(alist)
            len_top = len(alist)
            
            if len_top > n :
                len_top = int(len(alist)*25/100)
                
            count = 0
            for i in range(len_top):
                count+= alist[i]
            moy = int(count/len_top)
            newImage[c][d]=moy
    return newImage

#Correction de la luminance
def sweetLumi(image,n,seuil):
    height, width = image.shape
    for i in range(height):
        for j in range(width):
            count = 0
            for u in range(-n,(n+1)):
                for v in range(-n,(n+1)):
                    if u != 0 and v != 0 :
                        a=i+u
                        b=j+v
                        if a<0 or a>(height-1):
                            a=i-u
                        if b<0 or b>(width-1):
                            b=j-v
                        count = count + image[a][b]
            moy=count/((2*n+1)**2-1)
            if seuil>50 and seuil<=25:
                norm = 50/100
            else:
                norm = seuil/100
            if norm*moy> image[i][j]:
                image[i][j] = moy

#Amélioration de l image
#imglum c est l image resultant du calcul de la luminance
##def cleanImage(image, width, height, imglum):
##    hLum, wLum = imglum.shape
##    coef = int(bin(255)[2:]+ bin(255)[2:]+bin(255)[2:],2) // 255
##    n = height//hLum
##    if fmod(height,hLum) != 0:
##        n = n + 1
##    newImage = image
##    c = -1
##    for x in range (0,height,n) :
##        c=c+1
##        d=-1
##        for y in range (0,width,n) :
##            d=d+1
##            u=0
##            v=0
##            for u in range(n):
##                a = x+u
##                if a> (height-1):
##                    break;
##                for v in range(n):
##                    b = y+v
##                    if b> (width-1):
##                        break;
##                    imbin1 = bin(image[a][b][0])[2:]+bin(image[a][b][1])[2:]+bin(image[a][b][2])[2:]
##
##                    cinput = int(imbin1,2)
##                    imbin2 = bin(imglum[c][d])[2:]+ bin(imglum[c][d])[2:]+bin(imglum[c][d])[2:]
##                    clight = int(imbin2,2)
##                    cout = min(1,cinput/clight)
##                    cnew = 0.5-0.5*cos((cout**0.75)*np.pi)
##                    for i in range(3):
##                        newImage[a][b][i] = 255*cnew
##                    
##    return newImage

