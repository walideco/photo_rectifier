###########Cette Classe qui represente l'objet point###################

from math import *

class Point():
    def __init__(self,x,y):
        self.x = x
        self.y = y
    
    #Distance entre deux points
    def distance(self,point):
        return sqrt( (self.x - point.x)**2 + (self.y - point.y)**2  )

    def __str__(self):
        res = "Point("+ str(self.x) + ","+str(self.y)+")"
        return res
