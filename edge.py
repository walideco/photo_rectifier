############# Cette Classe qui represente l'objet droit################

from math import *
from point import *

class MyEdge ():
    
    #imgDim est une liste des dimensions de l'image avec pour
    #premier element height et second element width
    def __init__(self,rho,theta,imgDim):
        self.theta = theta
        self.rho = rho
        a = cos(self.theta)
        b = sin(self.theta)
        x0 = a*rho
        y0 = b*rho
        self.x1=int(x0  +int(max(imgDim[0],imgDim[1]) )*(-b))# x1
        self.y1=int(y0  + int(max(imgDim[0],imgDim[1]) )*( a))  #y1
        self.x2=int(x0 - int(max(imgDim[0],imgDim[1]) )*(-b))  #x2
        self.y2=int(y0 - int(max(imgDim[0],imgDim[1]) )*(a)) #y2

        self.point = Point(int(x0),int(y0))

        self.orientation = atan2(y0,x0) # ==> theta
        if self.orientation < 0 :
            self.orientation = self.orientation + 2 * pi

    def __str__(self):
        return "Edge["+str(self.rho)+" ; "+str(self.theta)+"]"
    
    #equation de droit sous la forme
    # y = ax+b
    # x = c
    def getEquation(self):
        v1 = (self.x2) - (self.x1) 
        v2 = (self.y2) - (self.y1)
        
        if v1== 0 :
            return [1,self.x1,0] # 1 pour dire que la droite est vertival soit equation x = c
        else :
            a = v2/v1 
            b = self.y1 - a*self.x1 
            return [0,a,b] # 0 pour une equation de la forme y = ax+b ou a peut etre null

    #determination du point d intercetion de deux droites
    def intersection ( self,edge):
        c1 = self.getEquation()
        c2 = edge.getEquation()
        
        if c1[0] == 0 and c2[0]==0 and (c1[1] - c2[1]) != 0 :
            x = int( (c2[2] - c1[2] ) / (c1[1] - c2[1] ))
            y = int(c1[1]*(c2[2] - c1[2] )/(c1[1] - c2[1] ) + c1[2])
        else :
            if c1[0] == 0 and c2[0] == 1 :
                x = int(c2[1]) 
                y = int(c1[1]*c2[1] + c1[2])
            else :
                if c2[0] == 0 and c1[0] == 1 :
                    x = int(c1[1])
                    y = int(c2[1]*c1[1] + c2[2])
                else :
                    x = 10000000
                    y = 10000000
        return Point(x,y)
        
    #Permet de verifier le parallelisme entre deux droits
    def checkOpposed(self,edge,imgDim): 
        if self.isEqual(edge) :
            return False
   
        p1=self.point
        p2=edge.point

        distance = p1.distance(p2)
              
        corner = self.orientation - edge.orientation
        if corner < 0 :
            corner = 2 * pi + corner
        if( ( (((pi - pi /6) < corner)  and (corner < (pi + pi /6) ) ) or
              ( ((2 * pi - pi /6) < corner and corner < 2 * pi)) or (0 <corner and corner < (0 + pi /6)) )
            and   (distance > max(imgDim[0],imgDim[1])/4)):
            return True
        return False

    #Permet de verifier l orthogonalite de deux droites
    def chekSuccessive( self, edge):
        corner = self.orientation - edge.orientation
        if(corner < 0):
            corner = 2 * pi + corner
        if ( ( (  (pi/2 - pi /6) < corner)  and (corner < (pi/2 + pi /6) ) ) or ( (  (3* pi/2 - pi /6) < corner)  and (corner < (3* pi/2 + pi /6) ) ) ):
            return True
        return False
    
    #Permet de derterminer l egalite entre deux droites
    def isEqual (self,edge):
        return ((self.theta == edge.theta) and (self.rho == edge.rho))
