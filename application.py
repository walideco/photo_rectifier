#_______________________________PROJET_PHYTON_2015/2016_________________________ #
 #____________________________Redressement_de tableau_blanc______________________ #
  #_________________________Walid_ETTAIEB_&_Jonathan_NIANGORAN________________#

import numpy as np
import cv2
##from matplotlib import pylab as plt
from math import *
import sys
from function import *
from point import *
from edge import *

def main(arg):
    try:
        #charger l image
        img0 = cv2.imread(arg)
        #convertir l image en niveau de gris
        gray = cv2.cvtColor(img0, cv2.COLOR_BGR2GRAY)
    except (cv2.error):
        print("Erreur d'argument")
        sys.exit(0)

    #recuperer les dimension de l image
    height, width = gray.shape
    
    #imgDim est une liste des dimensions de l image avec pour
    #premier element height et second element width
    imgDim = [height,width]
    
    print( "############REDRESSEMENT D IMAGE DE TAILLE :" + str(height) +"x "+str(width) +"############")

    # elliminer le bruit
    img = cv2.GaussianBlur(gray,(3,3),0)
              
    edges = cv2.Canny(img,10,115,apertureSize = 3,L2gradient=True)

    ##calcule du nombre de point blanc retourne par canny
    threshold1 = 0

    for x in range (0,height) :
        for y in range (0,width) :
            if edges[x][y] == 255 :
                threshold1 = threshold1 + 1

    print("Point blanc: ",threshold1)

    threshold1 = threshold1/4

    #calcule des ligne a partir des edges
    threshold1=int(threshold1*0.09)
    threshold2=int(sqrt(height**2 + width**2)*0.18)
    if threshold1 > threshold2:
        threshold = threshold2
    else :
        threshold = threshold1
        
    print("threshold : ",threshold)##rajouter une exeception

    houghLines = cv2.HoughLines(edges,1, np.pi/180,threshold)

    opposites = {}
    i = 0

    #Gestion de cas exceptionnels le programme s'arrete

    
    
    try:
        for index1 in range(0,len(houghLines)):
            d1 = MyEdge(houghLines[index1][0][0],houghLines[index1][0][1],imgDim) 
            for index2 in range(0,len(houghLines)):
                d2 = MyEdge(houghLines[index2][0][0],houghLines[index2][0][1],imgDim)
                if ( d1.checkOpposed (d2,imgDim) ):
                    if(d1.rho < d2.rho):
                        opposites[i] = [d1,d2]
                    else:
                        opposites[i] = [d2,d1]
                    i=i+1
    except TypeError:
        print("Cette image n'est pas supporte par l'application:")
        print("\t impossible de retrouver les droites opposees")
        sys.exit(0)
                
    i=0
    frames = {}
    for opposites1 in opposites.values():
        for opposites2 in opposites.values():
            d1 = opposites1[0]
            d2 = opposites2[1]
            d3 = opposites1[1]
            d4 = opposites2[0]
            if( (d1.isEqual(d4) == False) or (d3.isEqual(d2)==False) ):
                if( d1.chekSuccessive(d2) and d2.chekSuccessive(d3) and d3.chekSuccessive(d4) and d4.chekSuccessive(d1) ):
                    
                    circumference = getCircumference(d1,d2,d3,d4)                
                    testSpacing = checkSpacing(d1.intersection(d2),d2.intersection(d3) ,d3.intersection(d4) ,d4.intersection(d1),imgDim)
                    
                    if(testSpacing):  
                        frames[i] = [d1,d2,d3,d4,circumference]
                        i=i+1

    #choix de la plus petite quoidrilatère renvoyée
    try:
        minCircumf = width*height
        for frame in frames.values():
            if minCircumf > frame[4]:
                minCircumf = frame[4]
                res = frame
        e1 = res[0]
        e2 = res[1]
        e3 = res[2]
        e4 = res[3]
    except UnboundLocalError:
        print("Cette image n'est pas supporte par l'application:")
        print("\t impossible de detecter le quadrilatere")
        sys.exit(0)
    
    print(e1)
    print(e2)
    print(e3)
    print(e4)

    ##center = getQuadriCenter1(e1,e2,e3,e4)

    p1 = e1.intersection(e2)
    p2 = e2.intersection(e3)
    p3 = e3.intersection(e4)
    p4 = e4.intersection(e1)


    points = sortPoints (p1,p2,p3,p4)

    p3 = points[0]
    p4 = points[1]
    p1 = points[2]
    p2 = points[3]

    hMax = max( p1.distance( p3 ) , p2.distance( p4 ) )
    wMax = max( p1.distance( p2 ) , p3.distance( p4 ) )

    print("Quadrilatère détéctée est :")
    print(p1)
    print(p2)
    print(p3)
    print(p4)

    m1 = np.array ( [p1.x,p1.y,1] )
    m2 = np.array ( [p2.x,p2.y,1] )
    m3 = np.array ( [p3.x,p3.y,1] )
    m4 = np.array ( [p4.x,p4.y,1] )

    k2 = np.dot(np.cross(m1,m4) , m3 ) / np.dot( np.cross(m2,m4) , m3 )
    k3 = np.dot(np.cross(m1,m4) , m2 ) / np.dot( np.cross(m3,m4) , m2 )

    n2 =  k2 * m2 - m1
    n3 =  k3* m3 - m1

    centerOfImg =  Point(int(height/2),int(width/2))

    #f : la longueur focal

    #Gestion de cas exceptionnels le programme s'arrete
    try:
        f = sqrt((-1/(n2[2]*n3[2]) ) * ( n2[0]  * n3[0]  - ( n2[0] * n3[2]  + n2[2] * n3[0]) * centerOfImg.x + n2[2] * n3[2] * (centerOfImg.x**2) + n2[1] * n3[1] - (  n2[1] * n3[2] + n2[2] * n3[1]  ) * centerOfImg.y + n2[2] * n3[2] * ( centerOfImg.y**2)) )
    except ValueError:
        print("Cette image n'est pas supporte par l'application")
        sys.exit(0)
        
    print("distance focale trouvée = "+str(f))

    A = np.array([ [f , 0 , centerOfImg.x] , [ 0 , f , centerOfImg.y ] ,[0 , 0 , 1 ] ])
    inv_A  = np.linalg.inv(A)
    t_inv_A = np.transpose(inv_A)

    t_n2 = np.transpose(n2)
    t_n3 = np.transpose(n3)

    sc1 = np.dot(t_n2,np.dot(t_inv_A,np.dot(inv_A , n2)))
    sc2 = np.dot(t_n3,np.dot(t_inv_A,np.dot(inv_A , n3)))


    ratio = sqrt(sc1/sc2)
    print("le ratio trouvée="+str(ratio))

    if( (wMax / hMax) >= ratio ):
        final_w = int(wMax)
        final_h = int(final_w/ratio)
    else:
        final_h = int(hMax)
        final_w = int(final_h*ratio)
    print("les dimension de la nouvelle image sont:")
    print("H=" +str(final_h))
    print("W=" +str(final_w))

    points = sortPoints (p1,p2,p3,p4)

    p1 = points[0]
    p2 = points[1]
    p3 = points[2]
    p4 = points[3]


    pts1 = np.float32([ [p1.x,p1.y],[p2.x,p2.y],[p3.x,p3.y],[p4.x,p4.y] ])
    pts2 = np.float32( [ [0,0],[final_w,0], [0,final_h], [final_w,final_h] ] )

    M = cv2.getPerspectiveTransform(pts1,pts2)
    finalImage = cv2.warpPerspective( img0, M , (final_w, final_h) )
    imglum = lumImage(finalImage,final_w, final_h,15)
    sweetLumi(imglum,1,0)
##    imageClean = cleanImage(finalImage, width, height, imglum)
    cv2.imshow('Original image',img0)
    cv2.imshow('Final image',finalImage)
##    cv2.imshow('Clean image',imageClean)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == "__main__" and len(sys.argv) == 2:
    main(sys.argv[1])
else :
    print("Il nous faut une seule image en argument")
